package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.empty.EmailEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithEmailException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithLoginException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;
import ru.tsc.ichaplygina.taskmanager.marker.DatabaseCategory;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    @NotNull
    private IUserService userService;

    @NotNull
    private List<User> userList;

    @Before
    public void initTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        userService = new UserService(connectionService, propertyService);
        @NotNull final User admin = new User("admin", "admin", "admin@admin", "A.", "D.", "Min", Role.ADMIN);
        @NotNull final User user = new User("user", "user", "user@user", "U.", "S.", "Er", Role.USER);
        @NotNull final User cat = new User("cat", "cat", "cat@cat", "C.", "A.", "T", Role.USER);
        @NotNull final User mouse = new User("mouse", "mouse", "mouse@mouse", "M.", "O.", "Use", Role.USER);
        userList = new ArrayList<>();
        userList.add(admin);
        userList.add(user);
        userList.add(cat);
        userList.add(mouse);
        userService.add(admin);
        userService.add(user);
        userService.add(cat);
        userService.add(mouse);
        userService.lockByLogin("mouse");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testAdd() {
        Assert.assertEquals(4, userService.getSize());
        userService.add("dog", "dog", "dog@dog", Role.USER, "D.", "O.", "G");
        Assert.assertEquals(5, userService.getSize());
        userService.removeByLogin("dog");
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testAddEmptyLogin() {
        userService.add("", "dog", "dog@dog", Role.USER, "D.", "O.", "G");
    }

    @Test(expected = PasswordEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testAddEmptyPassword() {
        userService.add("dog", "", "dog@dog", Role.USER, "D.", "O.", "G");
    }

    @Test(expected = EmailEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testAddEmptyEmail() {
        userService.add("dog", "dog", "", Role.USER, "D.", "O.", "G");
    }

    @Test(expected = UserExistsWithLoginException.class)
    @Category(DatabaseCategory.class)
    public void testAddLoginExists() {
        userService.add("cat", "dog", "dog@dog", Role.USER, "D.", "O.", "G");
    }

    @Test(expected = UserExistsWithEmailException.class)
    @Category(DatabaseCategory.class)
    public void testAddEmailExists() {
        userService.add("dog", "dog", "cat@cat", Role.USER, "D.", "O.", "G");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testClear() {
        userService.clear();
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAll() {
        Assert.assertEquals(4, userService.findAll().size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindById() {
        @NotNull final String userId = userService.findAll().get(0).getId();
        Assert.assertNotNull(userService.findById(userId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByLogin() {
        Assert.assertNotNull(userService.findByLogin("admin"));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testFindByLoginUserNotFound() {
        userService.findByLogin("dog");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByLoginForAuthorization() {
        Assert.assertNotNull(userService.findByLoginForAuthorization("admin"));
    }

    @Test(expected = IncorrectCredentialsException.class)
    @Category(DatabaseCategory.class)
    public void testFindByLoginForAuthorizationUserNotFound() {
        userService.findByLoginForAuthorization("dog");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetSize() {
        Assert.assertEquals(4, userService.getSize());
        userService.add(new User("dog", "dog", "dog@dog", "M.", "O.", "Use", Role.USER));
        Assert.assertEquals(5, userService.getSize());
        userService.clear();
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsEmpty() {
        Assert.assertFalse(userService.isEmpty());
        userService.clear();
        Assert.assertTrue(userService.isEmpty());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsPrivilegedUser() {
        Assert.assertTrue(userService.isPrivilegedUser(userList.get(0).getId()));
        Assert.assertFalse(userService.isPrivilegedUser(userList.get(1).getId()));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testIsPrivilegedUserNotFound() {
        userService.isPrivilegedUser("unknown id");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testLockById() {
        @NotNull final String id = userList.get(0).getId();
        Assert.assertTrue(userService.lockById(id));
        Assert.assertTrue(userService.findById(id).isLocked());
    }

    @Test(expected = IdEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testLockByIdEmptyId() {
        userService.lockById("");
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testLockByIdUserNotFound() {
        userService.lockById("unknown id");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testLockByIdUserAlreadyLocked() {
        Assert.assertFalse(userService.lockById(userList.get(3).getId()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testLockByLogin() {
        Assert.assertTrue(userService.lockByLogin("admin"));
        Assert.assertTrue(userService.findByLogin("admin").isLocked());
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testLockByLoginEmptyLogin() {
        userService.lockByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testLockByLoginUserNotFound() {
        userService.lockByLogin("dog");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testLockByLoginUserAlreadyLocked() {
        Assert.assertFalse(userService.lockByLogin("mouse"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemoveByLogin() {
        Assert.assertNotNull(userService.removeByLogin("mouse"));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testRemoveByLoginNotFound() {
        userService.removeByLogin("dog");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testSetPassword() {
        @NotNull final String oldHash = userList.get(0).getPasswordHash();
        userService.setPassword("admin", "123");
        Assert.assertNotEquals(oldHash, userService.findByLogin("admin").getPasswordHash());
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testSetPasswordEmptyLogin() {
        userService.setPassword("", "password");
    }

    @Test(expected = PasswordEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testSetPasswordEmptyPassword() {
        userService.setPassword("login", "");
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testSetPasswordUserNotFound() {
        userService.setPassword("login", "password");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testSetRole() {
        userService.setRole("user", Role.ADMIN);
        Assert.assertTrue(userService.isPrivilegedUser(userService.findByLogin("user").getId()));
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testSetRoleEmptyLogin() {
        userService.setRole("", Role.ADMIN);
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testSetRoleUserNotFound() {
        userService.setRole("odmin", Role.ADMIN);
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUnlockById() {
        @NotNull final String id = userList.get(3).getId();
        Assert.assertTrue(userService.unlockById(id));
        Assert.assertFalse(userService.findById(id).isLocked());
    }

    @Test(expected = IdEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUnlockByIdEmptyId() {
        userService.unlockById("");
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testUnlockByIdUserNotFound() {
        userService.unlockById("unknown id");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUnlockByIdUserAlreadyUnlocked() {
        Assert.assertFalse(userService.unlockById(userList.get(0).getId()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUnlockByLogin() {
        Assert.assertTrue(userService.unlockByLogin("mouse"));
        Assert.assertFalse(userService.findByLogin("mouse").isLocked());
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUnlockByLoginEmptyLogin() {
        userService.unlockByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testUnlockByLoginUserNotFound() {
        userService.unlockByLogin("dog");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUnlockByLoginUserAlreadyUnlocked() {
        Assert.assertFalse(userService.unlockByLogin("admin"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUpdateById() {
        @NotNull final User userExpected = userList.get(0);
        @NotNull final String userId = userExpected.getId();
        @NotNull final String newLogin = "new_login";
        @NotNull final String newPassword = "new_password";
        @NotNull final String newEmail = "new_email1";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userExpected.setLogin(newLogin);
        userExpected.setPasswordHash(newPassword);
        userExpected.setEmail(newEmail);
        userExpected.setRole(newRole);
        userExpected.setFirstName(newFirstName);
        userExpected.setMiddleName(newMiddleName);
        userExpected.setLastName(newLastName);
        @Nullable final User userActual = userService.updateById(userId, newLogin, newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
        Assert.assertEquals(userExpected.getId(), userActual.getId());
        Assert.assertEquals(userExpected.getLogin(), userActual.getLogin());
        Assert.assertEquals(userExpected.getPasswordHash(), userActual.getPasswordHash());
        Assert.assertEquals(userExpected.getEmail(), userActual.getEmail());
        Assert.assertEquals(userExpected.getRole(), userActual.getRole());
        Assert.assertEquals(userExpected.getFirstName(), userActual.getFirstName());
        Assert.assertEquals(userExpected.getMiddleName(), userActual.getMiddleName());
        Assert.assertEquals(userExpected.getLastName(), userActual.getLastName());
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByIdEmptyLogin() {
        @NotNull final User userExpected = userList.get(0);
        @NotNull final String userId = userExpected.getId();
        @NotNull final String newLogin = "";
        @NotNull final String newPassword = "new_password";
        @NotNull final String newEmail = "new_email2";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateById(userId, newLogin, newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test(expected = PasswordEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByIdEmptyPassword() {
        @NotNull final User userExpected = userList.get(0);
        @NotNull final String userId = userExpected.getId();
        @NotNull final String newLogin = "login";
        @NotNull final String newPassword = "";
        @NotNull final String newEmail = "new_email3";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateById(userId, newLogin, newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test(expected = UserExistsWithLoginException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByIdUserExistsWithLogin() {
        @NotNull final User userExpected = userList.get(0);
        @NotNull final String userId = userExpected.getId();
        @NotNull final String newLogin = "user";
        @NotNull final String newPassword = "password";
        @NotNull final String newEmail = "email";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateById(userId, newLogin, newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test(expected = UserExistsWithEmailException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByIdUserExistsWithEmail() {
        @NotNull final User userExpected = userList.get(0);
        @NotNull final String userId = userExpected.getId();
        @NotNull final String newLogin = "login";
        @NotNull final String newPassword = "password";
        @NotNull final String newEmail = "user@user";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateById(userId, newLogin, newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByIdUserNotFound() {
        @NotNull final String userId = "123";
        @NotNull final String newLogin = "login";
        @NotNull final String newPassword = "password";
        @NotNull final String newEmail = "email";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateById(userId, newLogin, newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUpdateByLogin() {
        @NotNull final User userExpected = userList.get(0);
        @NotNull final String newPassword = "new_password";
        @NotNull final String newEmail = "new_email4";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userExpected.setPasswordHash(newPassword);
        userExpected.setEmail(newEmail);
        userExpected.setRole(newRole);
        userExpected.setFirstName(newFirstName);
        userExpected.setMiddleName(newMiddleName);
        userExpected.setLastName(newLastName);
        @Nullable final User userActual = userService.updateByLogin("admin", newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
        Assert.assertEquals(userExpected.getId(), userActual.getId());
        Assert.assertEquals(userExpected.getLogin(), userActual.getLogin());
        Assert.assertEquals(userExpected.getPasswordHash(), userActual.getPasswordHash());
        Assert.assertEquals(userExpected.getEmail(), userActual.getEmail());
        Assert.assertEquals(userExpected.getRole(), userActual.getRole());
        Assert.assertEquals(userExpected.getFirstName(), userActual.getFirstName());
        Assert.assertEquals(userExpected.getMiddleName(), userActual.getMiddleName());
        Assert.assertEquals(userExpected.getLastName(), userActual.getLastName());
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByLoginEmptyLogin() {
        @NotNull final User userExpected = userList.get(0);
        @NotNull final String newPassword = "new_password";
        @NotNull final String newEmail = "new_email5";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateByLogin("", newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test(expected = PasswordEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByLoginEmptyPassword() {
        @NotNull final User userExpected = userList.get(0);
        @NotNull final String newPassword = "";
        @NotNull final String newEmail = "new_email6";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateByLogin("admin", newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test(expected = UserExistsWithEmailException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByLoginUserExistsWithEmail() {
        @NotNull final String newPassword = "password";
        @NotNull final String newEmail = "admin@admin";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateByLogin("cat", newPassword, newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByLoginUserNotFound() {
        @NotNull final String newPassword = "password";
        @NotNull final String newEmail = "admin@admin";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateByLogin("vasya", newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @After
    public void clean() {
        for (@NotNull final User user : userService.findAll()) {
            if (user.getLogin().equals("user") || user.getLogin().equals("admin") || user.getLogin().equals("new_login")
                    || user.getLogin().equals("mouse") || user.getLogin().equals("cat") || user.getLogin().equals("dog"))
                userService.removeById(user.getId());
        }
    }

}
