package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.api.service.*;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.marker.DatabaseCategory;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceTest {

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @NotNull
    private IProjectTaskService projectTaskService;

    @NotNull
    private List<Task> taskList;

    @Before
    public void initTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        userService = new UserService(connectionService, propertyService);
        projectService = new ProjectService(connectionService, userService);
        taskService = new TaskService(connectionService, userService);
        projectTaskService = new ProjectTaskService(taskService, projectService);
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        @NotNull final User admin = new User("admin", "admin", "admin@admin", "A.", "D.", "Min", Role.ADMIN);
        @NotNull final User user = new User("user", "user", "user@user", "U.", "S.", "Er", Role.USER);
        userService.add(admin);
        userService.add(user);
        projectList.add(new Project("Admin Project 1", "", admin.getId()));
        projectList.add(new Project("Admin Project 2", "", admin.getId()));
        projectList.add(new Project("User Project 1", "", user.getId()));
        projectList.add(new Project("User Project 2", "", user.getId()));
        taskList.add(new Task("Admin Task 1", "", admin.getId()));
        taskList.add(new Task("Admin Task 2", "", admin.getId()));
        taskList.add(new Task("User Task 1", "", user.getId()));
        taskList.add(new Task("User Task 2", "", user.getId()));
        for (@NotNull final Project project : projectList) projectService.add(project);
        for (@NotNull final Task task : taskList) taskService.add(task);
        for (int i = 0; i < 4; i++)
            taskService.addTaskToProject(admin.getId(), taskList.get(i).getId(), projectList.get(i).getId());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testAdd() {
        final int expectedSize = projectService.getSize() + 1;
        projectService.add(userService.findByLogin("user").getId(), "123", "123");
        Assert.assertEquals(expectedSize, projectService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testAddAll() {
        final int expectedSize = projectService.getSize() + 10;
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull List<Project> projectList = new ArrayList<>();
        for (int i = 1; i <= 10; i++) projectList.add(new Project("project" + i, "123", userId));
        projectService.addAll(projectList);
        Assert.assertEquals(expectedSize, projectService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testClearAdmin() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String adminUserId = userService.findByLogin("user").getId();
        projectService.clear(adminUserId);
        Assert.assertEquals(0, projectService.getSize(adminUserId));
        Assert.assertEquals(0, projectService.getSize(userId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testClearUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertNotEquals(0, projectService.getSize(userId));
        projectService.clear(userId);
        Assert.assertEquals(0, projectService.getSize(userId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testCompleteById() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(projectService.completeById(userId, projectList.get(0).getId()));
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectList.get(0).getId()).getStatus());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testCompleteByName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(projectService.completeByName(userId, projectList.get(0).getName()));
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectList.get(0).getId()).getStatus());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testCompleteByIdWrongUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertNull(projectService.completeById(userId, projectList.get(0).getId()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testCompleteByNameWrongUser() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNull(projectService.completeById(userId, projectList.get(0).getName()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAllAdmin() {
        final int expectedSize = projectService.getSize();
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull List<Project> projectList = projectService.findAll(userId);
        Assert.assertEquals(expectedSize, projectList.size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAllUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull List<Project> projectList = projectService.findAll(userId);
        Assert.assertEquals(2, projectList.size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIdAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertEquals(projectList.get(0).getId(), projectService.findById(userId, projectId).getId());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIdUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertNull(projectService.findById(userId, projectId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByNameAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "Admin Project 1";
        Assert.assertEquals(projectList.get(0).getId(), projectService.findByName(userId, projectName).getId());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByNameUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String projectName = "Admin Project 1";
        Assert.assertNull(projectService.findByName(userId, projectName));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetIdByNameAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "User Project 1";
        Assert.assertEquals(projectList.get(2).getId(), projectService.getId(userId, projectName));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetIdByNameUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String projectName = "Admin Project 1";
        Assert.assertNull(projectService.getId(userId, projectName));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetSizeAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertEquals(4, projectService.getSize(userId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetSizeUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertEquals(2, projectService.getSize(userId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsEmptyAdmin() {
        @NotNull final String adminUserId = userService.findByLogin("admin").getId();
        @NotNull final String userUserId = userService.findByLogin("user").getId();
        Assert.assertFalse(projectService.isEmpty(adminUserId));
        projectService.clear(adminUserId);
        Assert.assertTrue(projectService.isEmpty(adminUserId));
        Assert.assertTrue(projectService.isEmpty(userUserId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsEmptyUser() {
        @NotNull final String adminUserId = userService.findByLogin("admin").getId();
        @NotNull final String userUserId = userService.findByLogin("user").getId();
        Assert.assertFalse(projectService.isEmpty(userUserId));
        projectService.clear(userUserId);
        Assert.assertTrue(projectService.isEmpty(userUserId));
        Assert.assertFalse(projectService.isEmpty(adminUserId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemovedByIdAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertEquals(projectList.get(0).getId(), projectService.removeById(userId, projectId).getId());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemoveByIdUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertNull(projectService.removeById(userId, projectId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemoveByNameAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "Admin Project 1";
        Assert.assertEquals(projectList.get(0).getId(), projectService.removeByName(userId, projectName).getId());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemoveByNameUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String projectName = "Admin Project 1";
        Assert.assertNull(projectService.removeByName(userId, projectName));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testStartById() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertNotNull(projectService.startById(userId, projectId));
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(projectId).getStatus());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testStartByName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(projectService.startByName(userId, projectList.get(0).getName()));
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(projectList.get(0).getId()).getStatus());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testStartByIdWrongUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertNull(projectService.startById(userId, projectList.get(0).getId()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testStartByNameWrongUser() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNull(projectService.startById(userId, projectList.get(0).getName()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUpdateById() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertNotNull(projectService.updateById(userId, projectId, "new name", "new description"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUpdateByIdUnknownId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "???";
        Assert.assertNull(projectService.updateById(userId, projectId, "new name", "new description"));
    }

    @Test(expected = IdEmptyException.class)
    @Category(DatabaseCategory.class)

    public void testUpdateByIdEmptyId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "";
        projectService.updateById(userId, projectId, "new name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    @Category(DatabaseCategory.class)

    public void testUpdateByIdEmptyName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        projectService.updateById(userId, projectId, "", "new description");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testCompleteProjectById() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertNotNull(projectService.completeById(userId, projectId));
    }

    @Test(expected = IdEmptyException.class)
    @Category(DatabaseCategory.class)

    public void testCompleteProjectByIdEmptyId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "";
        Assert.assertNotNull(projectService.completeById(userId, projectId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testCompleteProjectByIdUnknownId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "???";
        Assert.assertNull(projectService.completeById(userId, projectId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testCompleteProjectByName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = projectList.get(0).getName();
        Assert.assertNotNull(projectService.completeByName(userId, projectName));
    }

    @Test(expected = NameEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testCompleteProjectByNameEmptyName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "";
        projectService.completeByName(userId, projectName);
    }

    @Test
    @Category(DatabaseCategory.class)
    public void tesCompleteProjectByNameUnknownName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "???";
        Assert.assertNull(projectService.completeByName(userId, projectName));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testAddTaskToProject() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        @NotNull final String projectId = projectList.get(1).getId();
        Assert.assertNotNull(projectTaskService.addTaskToProject(userId, projectId, taskId));
    }

    @Test(expected = IdEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testAddTaskToProjectEmptyTaskId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = "";
        @NotNull final String projectId = projectList.get(1).getId();
        projectTaskService.addTaskToProject(userId, taskId, projectId);
    }

    @Test(expected = IdEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testAddTaskToProjectEmptyProjectId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        @NotNull final String projectId = "";
        projectTaskService.addTaskToProject(userId, taskId, projectId);
    }

    @Test(expected = ProjectNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testAddTaskToProjectUnknownProject() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String taskId = taskList.get(2).getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertNotNull(projectTaskService.addTaskToProject(userId, taskId, projectId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testClearProjectsAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        projectTaskService.clearProjects(userId);
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testClearProjectsUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        final int expectedProjectSize = projectService.getSize() - 2;
        final int expectedTaskSize = taskService.getSize() - 2;
        projectTaskService.clearProjects(userId);
        Assert.assertEquals(expectedProjectSize, projectService.getSize());
        Assert.assertEquals(expectedTaskSize, taskService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAllTasksByProjectId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertEquals(1, projectTaskService.findAllTasksByProjectId(userId, projectId, "").size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemoveProjectById() {
        final int expectedProjectSize = projectService.getSize() - 1;
        final int expectedTaskSize = taskService.getSize() - 1;
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        projectTaskService.removeProjectById(userId, projectId);
        Assert.assertEquals(expectedProjectSize, projectService.getSize(userId));
        Assert.assertEquals(expectedTaskSize, taskService.getSize(userId));
    }

    @Test(expected = IdEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testRemoveProjectByIdEmptyId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "";
        projectTaskService.removeProjectById(userId, projectId);
    }

    @Test(expected = ProjectNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testRemoveProjectByIdBadId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "123";
        projectTaskService.removeProjectById(userId, projectId);
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemoveProjectByName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        final int expectedProjectSize = projectService.getSize(userId) - 1;
        final int expectedTaskSize = taskService.getSize(userId) - 1;
        @NotNull final String projectName = projectList.get(0).getName();
        projectTaskService.removeProjectByName(userId, projectName);
        Assert.assertEquals(expectedProjectSize, projectService.getSize(userId));
        Assert.assertEquals(expectedTaskSize, taskService.getSize(userId));
    }

    @Test(expected = NameEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testRemoveProjectByNameEmptyName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "";
        projectTaskService.removeProjectByName(userId, projectName);
    }

    @Test(expected = ProjectNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testRemoveProjectByNameBadName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "123";
        projectTaskService.removeProjectByName(userId, projectName);
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemoveTaskFromProject() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String taskId = taskList.get(0).getId();
        Assert.assertNotNull(projectTaskService.removeTaskFromProject(userId, projectId, taskId));
    }

    @Test(expected = IdEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testRemoveTaskFromProjectIdEmpty() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "";
        @NotNull final String taskId = taskList.get(0).getId();
        projectTaskService.removeTaskFromProject(userId, projectId, taskId);
    }

    @Test(expected = IdEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testRemoveTaskFromProjectTaskIdEmpty() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String taskId = "";
        projectTaskService.removeTaskFromProject(userId, projectId, taskId);
    }

    @Test(expected = ProjectNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testRemoveTaskFromProjectNotFound() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = "234";
        @NotNull final String taskId = taskList.get(0).getId();
        projectTaskService.removeTaskFromProject(userId, projectId, taskId);
    }

    @After
    public void clean() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String adminUserId = userService.findByLogin("admin").getId();
        projectTaskService.clearProjects(userId);
        taskService.clear(userId);
        for (@NotNull final Project project : projectService.findAll(adminUserId)) {
            if (adminUserId.equals(project.getUserId()))
                projectTaskService.removeProjectById(adminUserId, project.getId());
        }
        for (@NotNull final Task task : taskService.findAll(adminUserId)) {
            if (adminUserId.equals(task.getUserId()))
                taskService.removeById(adminUserId, task.getId());
        }
        userService.removeByLogin("user");
        userService.removeByLogin("admin");
    }

}
