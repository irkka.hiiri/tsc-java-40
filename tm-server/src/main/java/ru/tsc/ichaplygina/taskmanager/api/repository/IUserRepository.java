package ru.tsc.ichaplygina.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (ID, LOGIN, PASSWORD, EMAIL, ROLE, FIRST_NAME, MIDDLE_NAME, LAST_NAME, LOCKED) " +
            "VALUES(#{id}, #{login}, #{password}, #{email}, #{role}, #{firstName}, #{middleName}, #{lastName}, #{locked})")
    void add(@Param("id") @NotNull String id,
             @Param("login") @NotNull String login,
             @Param("password") @NotNull String password,
             @Param("email") @NotNull String email,
             @Param("role") @NotNull Role role,
             @Param("firstName") @Nullable String firstName,
             @Param("middleName") @Nullable String middleName,
             @Param("lastName") @Nullable String lastName,
             @Param("locked") boolean locked);

    @Delete("DELETE FROM tm_user")
    void clear();

    @NotNull
    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "passwordHash", column = "PASSWORD"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "role", column = "ROLE"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "locked", column = "LOCKED")
    })
    List<User> findAll();

    @Nullable
    @Select("SELECT * FROM tm_user WHERE EMAIL = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "passwordHash", column = "PASSWORD"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "role", column = "ROLE"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "locked", column = "LOCKED")
    })
    User findByEmail(@Param("email") @NotNull String email);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE ID = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "passwordHash", column = "PASSWORD"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "role", column = "ROLE"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "locked", column = "LOCKED")
    })
    User findById(@Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE LOGIN = #{login} LIMIT 1")
    @Result(property = "id", column = "ID")
    @Result(property = "login", column = "LOGIN")
    @Result(property = "passwordHash", column = "PASSWORD")
    @Result(property = "email", column = "EMAIL")
    @Result(property = "role", column = "ROLE")
    @Result(property = "firstName", column = "FIRST_NAME")
    @Result(property = "middleName", column = "MIDDLE_NAME")
    @Result(property = "lastName", column = "LAST_NAME")
    @Result(property = "locked", column = "LOCKED")
    User findByLogin(@Param("login") @NotNull String login);

    @Nullable
    @Select("SELECT ID FROM tm_user WHERE EMAIL = #{login} LIMIT 1")
    String findIdByEmail(@Param("login") @NotNull String login);

    @Nullable
    @Select("SELECT ID FROM tm_user WHERE LOGIN = #{login} LIMIT 1")
    String findIdByLogin(@Param("login") @NotNull String login);

    @Select("SELECT COUNT(1) FROM tm_user")
    int getSize();

    @Delete("DELETE FROM tm_user WHERE ID = #{id}")
    void removeById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM tm_user WHERE LOGIN = #{login}")
    void removeByLogin(@Param("login") @NotNull String login);

    @Update("UPDATE tm_user SET LOCKED = #{locked} WHERE ID = #{id}  AND LOCKED <> #{locked}")
    int setLockedById(@Param("id") @NotNull String id, @Param("locked") boolean locked);

    @Update("UPDATE tm_user SET LOCKED = #{locked} WHERE LOGIN = #{login} AND LOCKED <> #{locked}")
    int setLockedByLogin(@Param("login") @NotNull String login, @Param("locked") boolean locked);

    @Update("UPDATE tm_user SET PASSWORD = #{password} WHERE ID = #{id}")
    void setPassword(@Param("id") @NotNull String id, @Param("password") @NotNull String password);

    @Update("UPDATE tm_user SET ROLE = #{role} WHERE ID = #{id}")
    void setRole(@Param("id") @NotNull String id, @Param("role") @NotNull Role role);

    @Update("UPDATE tm_user SET LOGIN = #{login}, PASSWORD = #{password}, EMAIL = #{email}, " +
            "ROLE = #{role}, FIRST_NAME = #{firstName}, MIDDLE_NAME = #{middleName}, LAST_NAME = #{lastName} " +
            "WHERE ID = #{id}")
    @Nullable int update(@Param("id") @NotNull String id,
                         @Param("login") @NotNull String login,
                         @Param("password") @NotNull String password,
                         @Param("email") @NotNull String email,
                         @Param("role") @NotNull Role role,
                         @Param("firstName") @Nullable String firstName,
                         @Param("middleName") @Nullable String middleName,
                         @Param("lastName") @Nullable String lastName);

}
