package ru.tsc.ichaplygina.taskmanager.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.ISessionRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.ISessionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.SessionNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedException;
import ru.tsc.ichaplygina.taskmanager.model.Session;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.List;
import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.HashUtil.salt;
import static ru.tsc.ichaplygina.taskmanager.util.SignatureUtil.sign;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public final class SessionService extends AbstractService<Session> implements ISessionService {


    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserService userService;

    public SessionService(@NotNull final IConnectionService connectionService,
                          @NotNull final IPropertyService propertyService,
                          @NotNull final IUserService userService) {
        super(connectionService);
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final Session session) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.add(session.getId(), session.getUserId(), session.getSignature(), session.getTimeStamp());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void addAll(@Nullable List<Session> sessionList) {
        if (sessionList == null) return;
        for (final Session session : sessionList) add(session);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public final void closeSession(@NotNull final Session session) {
        removeById(session.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return sessionRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return sessionRepository.findById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return sessionRepository.getSize();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @NotNull
    @Override
    @SneakyThrows
    public final Session openSession(@NotNull final String login, @NotNull final String password) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            @NotNull final User user = Optional.ofNullable(userService.findByLoginForAuthorization(login)).orElseThrow(IncorrectCredentialsException::new);
            if (!user.getPasswordHash().equals(salt(password, propertyService)))
                throw new IncorrectCredentialsException();
            if (user.isLocked()) throw new AccessDeniedException();
            @NotNull final Session session = new Session(user.getId());
            sign(session, propertyService);
            sessionRepository.add(session.getId(), session.getUserId(), session.getSignature(), session.getTimeStamp());
            sqlSession.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        @Nullable final Session session = Optional.ofNullable(sessionRepository.findById(id)).orElseThrow(SessionNotFoundException::new);
        try {
            sessionRepository.removeById(id);
            sqlSession.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public final void validatePrivileges(@NotNull final String userId) {
        if (!userService.isPrivilegedUser(userId))
            throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public final void validateSession(@Nullable final Session session) {
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        @Nullable final String signature = session.getSignature();
        Optional.ofNullable(signature).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(findById(session.getId())).orElseThrow(AccessDeniedException::new);
        @NotNull final Session sessionClone = session.clone();
        if (!signature.equals(sign(sessionClone, propertyService).getSignature()))
            throw new AccessDeniedException();
    }

}
