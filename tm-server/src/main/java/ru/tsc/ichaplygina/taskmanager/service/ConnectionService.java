package ru.tsc.ichaplygina.taskmanager.service;

import org.apache.ibatis.datasource.unpooled.UnpooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.property.IDatabaseProperty;
import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.repository.ISessionRepository;
import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.repository.IUserRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;

import javax.sql.DataSource;

public class ConnectionService implements IConnectionService {

    @NotNull
    final IDatabaseProperty databaseProperties;

    public ConnectionService(@NotNull IDatabaseProperty databaseProperties) {
        this.databaseProperties = databaseProperties;
    }

    @NotNull
    @Override
    public SqlSession getSqlSession() {
        return getSqlSessionFactory().openSession();
    }

    @NotNull
    @Override
    public SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String user = databaseProperties.getDatabaseUsername();
        @Nullable final String password = databaseProperties.getDatabasePassword();
        @Nullable final String url = databaseProperties.getDatabaseUrl();
        @Nullable final String driver = databaseProperties.getDatabaseDriver();
        final DataSource dataSource = new UnpooledDataSource(driver, url, user, password);
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
